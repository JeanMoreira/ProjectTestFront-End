import { JobDetalheComponent } from './job-detalhe/job-detalhe.component';
import { JobModule } from './job/job-obj.module';
import { RouteModule } from './route.module';



import { JobService } from './job.service';
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';

import { AppComponent } from './app.component';
import { JobComponent } from './job/job.component';

@NgModule({
  declarations: [
    AppComponent,
    JobDetalheComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    JobModule,
    HttpModule,
    RouteModule
  ],
  providers: [JobService],
  bootstrap: [AppComponent]
})
export class AppModule { }
