export class JobModule { 
  id:number;
  name:string;
  active:boolean;
  parent:JobModule;
  
  
  constructor( values: Object = {}) {
    Object.assign(this, values);
  }

}
