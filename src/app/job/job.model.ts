export class Job { 
    
    constructor(public id:number ,
        public name:string ,
        public active:boolean ,
        public parent:Job) {
    }
    

}