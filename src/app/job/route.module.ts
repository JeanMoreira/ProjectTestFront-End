import { JobDetalheComponent } from './../job-detalhe/job-detalhe.component';
import { JobComponent } from './job.component';

import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';

const routes: Routes = [
    {
        path: 'job',
        component: JobComponent
    },  
    {
        path: 'job/save',
        component: JobDetalheComponent
    },
    {
        path: 'job/:id',
        component: JobDetalheComponent
    }
  
];
@NgModule({
  imports: [
      RouterModule.forChild(routes)
  ],
  exports: [
      RouterModule
  ],
  declarations: []
})
export class RouteModuleJob { }
