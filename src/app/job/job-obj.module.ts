import { RouteModuleJob } from './route.module';

import { FormsModule } from '@angular/forms';
import { JobComponent } from './job.component';

import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

@NgModule({
  imports: [
    CommonModule,
    RouteModuleJob,
    FormsModule
  ],
  declarations: [JobComponent],
  exports:[JobComponent]
})
export class JobModule { 
  id:number;
  name:string;
  active:boolean;
  parent:JobModule;
  
  constructor( ) {
    
  }
}
