import { JobModule } from './job.module';
import { Job } from './job.model';
import { JobService } from './../job.service';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router/src/router';
import { ActivatedRoute } from '@angular/router/src/router_state';



@Component({
  selector: 'app-job',
  templateUrl: './job.component.html',
  styleUrls: ['./job.component.css']
})
export class JobComponent implements OnInit {


  jobs: JobModule[];
  constructor(
    private api: JobService
  ) { }

  ngOnInit() {
    this.getJob();
    
  }

  getJob(): void{
    this.api
    .getAllTodos().subscribe((jobs) => {
        this.jobs = jobs;
        console.log(this.jobs);
      }
    );

  }

}
