import { Job } from './job/job.model';
import { JobModule } from './job/job.module';
import { Http, RequestOptions,Headers } from '@angular/http';
import { Response } from '@angular/http';
import { Injectable } from '@angular/core';
import { environment } from '../environments/environment.prod';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import 'rxjs/add/observable/throw';


const API_URL = environment.apiUrl; 
@Injectable()
export class JobService {

  constructor(private http: Http) { 
    
  }

  private handleError (error: Response | any) {
    console.error('ApiServiceJob::handleError', error);
    return Observable.throw(error);
  }
 // API: GET /Prato
  public getAllTodos(): Observable<JobModule[]>  {
    // will use this.http.get()
    return this.http
    .get(API_URL + '/jobs')
    .map(response => {
      const pratos = response.json();
      return response.json();
    })
    .catch(this.handleError);
  }




  private extractData(res: Response) {
    console.log("Erro!")
    return new JobModule(res.json());
  }

  // API: POST /todos
  public createTodo(todo: JobModule): Observable<JobModule>  {
    let body = JSON.stringify(todo);
    console.log(body);
    let headers = new Headers();
    headers.append('Content-Type', 'application/json');
   
    let options = new RequestOptions({ headers: headers });
    return this.http
    .post(API_URL + '/jobs', todo, options)
    .map(response => {
      this.extractData;
    })
    .catch(this.handleError);
  }

  // API: GET /todos/:id
  public getTodoById(todoId: number) : Observable<JobModule> {
 
    return this.http
    .get(API_URL + '/jobs/' + todoId)
    .map(response => {
      return new JobModule(response.json());
    })
    .catch(this.handleError);
  }

  // API: PUT /todos/:id
  public updateTodo(todo: JobModule): Observable<JobModule> {
    return this.http
    .put(API_URL + '/jobs/' + todo.id, todo)
    .map(response => {
      return new JobModule(response.json());
    })
    .catch(this.handleError);
  }

  // DELETE /todos/:id
  public deleteTodoById(todoId: number) : Observable<null>{
    return this.http
    .delete(API_URL + '/jobs/' + todoId)
    .map(response => null)
    .catch(this.handleError);
  }

}
