import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { JobDetalheComponent } from './job-detalhe.component';

describe('RestauranteDetalheComponent', () => {
  let component: JobDetalheComponent;
  let fixture: ComponentFixture<JobDetalheComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ JobDetalheComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(JobDetalheComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
