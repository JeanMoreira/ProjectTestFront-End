import { Job } from './../job/job.model';
import { JobService } from './../job.service';
import { JobModule } from './../job/job.module';

import { Component, OnInit } from '@angular/core';


import { ActivatedRoute, Params,Router } from '@angular/router';

import { Location } from '@angular/common'




@Component({
  selector: 'app-job-detalhe',
  templateUrl: './job-detalhe.component.html',
  styleUrls: ['./job-detalhe.component.css']
})
export class JobDetalheComponent implements OnInit {
  jobModule: JobModule;

  constructor(
    private api: JobService,
    private route: ActivatedRoute,
    private location: Location,
    private router: Router,
  ) { }

  ngOnInit() {
    this.route.params.forEach((params:Params)=>{
      
      let id: number = +params['id'];
      if(id){
        this.api
        .getTodoById(id).subscribe((job) => {
            this.jobModule = job;
          }
        );
      }
      this.jobModule = new Job(0,"",true,null);
    })
  }


  onSubmit(): void{
    if(!this.jobModule.id){
      this.api.createTodo(this.jobModule).subscribe((job) => {
        console.log(this.jobModule);
        }
      );

    }else{
      this.api.updateTodo(this.jobModule).subscribe((job) => {
        console.log(this.jobModule);
        }
      );
    }
    
    

    this.router.navigateByUrl('/job');
    this.router.navigate(["job"]);
  }

}
